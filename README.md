# This is a minimal version of the project ! Initially created for personal use but turned in as a project for the MECA653 module at Polytech Annecy. Created by Antoine Leroux

This project is not done. It still needs some improvements and optimizations but the foundations are there.


# How to run `Final_Product_for_realtime_OCR.ipynb` ?

- This notebook launch one thread for each camera (or whatever video source) and can use GPU Acceleration with CUDA.
- This notebook works well in Jupyter Notebook. You need to install ipyleaflet and ipywidgets extensions so that the city_map actuaize itself and for interact check box to work.

<ins>You need to define :</ins>
    
- The License Plate you're looking for :

        plate_to_search = 'FG868AL'

- The center location of your city map :

        center = [45.921510, 6.160615]

- The zoom of our map :

        zoom = 13
        
- The geographical area of your map :

        Area = [45.907988, 45.931551, 6.122234, 6.178818]

- If you want to use GPU (CUDA Acceleration) for a faster inference (Need to install all requiered packages and build OpenCV to use your graphic card) :

        use_GPU = True


There is a demo run. If you don't want to use multithreading (Multithreading is a little glitchy with Keras), you can run the following :

    Cam_Apercu("Source 1", 1, False, True, plates_dnn_TensorflowNet_pb,plates_dnn_TensorflowNet_pbtxt,vehicles_dnn_caffe_prototxt,vehicles_dnn_caffe_model,cam_network_db)

where :
- the second argument is the ID of the video source saved in the Database (must be an integer).
- the third one is to tell our main function not to run multithreaded (True or False).
- the fourth one is to tell our main function to use GPU CUDA Acceleration (True or False).

<ins>**PS :**</ins> If you want to display the video file, edit `display` variable in `Cam_Apercu` function and set it to True. Press q to quit when running.


Instead, if you execute the Main() function, it's going to fetch all Cameras' ID in the Database and launch one thread for each one of them (Hardware ressources needed ! Made to be run on multiple servers) and update the city_map with markers of where your License Plate has been seen.

**Warning :** A checkbox is present at the execution of `Main()` that allows you to shut down the system completely.


**<ins>When the `Main()` function is launched, you'll have this :</ins>**

![Launching Threading System](docs/Starting_Threading_System.png)

**<ins>Then when you'll stop it by check the box (setting stop_threads variable to True) you'll have this and the execution totally stopped :</ins>**

![Stopping Threading System](docs/Stopping_Threading_System.png)

**<ins>Map display of the markers with the video file example on the interactive map :</ins>**

![Map](docs/Map_Demo.png)


**To obtain the same result as above, just run all the notebook's cells except the last one !**


# Setup

## Standard packages installation using pip


        pip install -r example-requirements.txt
 
    